﻿$(function() {
    var Findstr;
    $("#in1").bind('keyup', function() {
        if (document.getElementById("in1").value.length > 2){
            Findstr = document.getElementById("in1").value;
            SendGet(Findstr);
        }
    })
});
function SendGet(par) {
    console.log("Connection started");

    $.ajax({

        url: "http://cstrade.koenig.ru:7001/UrbanViewAPI/rest/address/suggest",
        encoding: "UTF-8",
        type: "GET",
        dataType: 'json',
        async: true,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function() {

            var d = new Date();
            start_time = d.getTime();
        },

        data: {
            value: par,
            schema: "KALININGRAD_UAG"
        },

        error: function (xhr, status, error) {
            console.log(error);
        },

        success: function (response) {
            console.log('Success');
            var d = new Date();

            $( "#in1" ).autocomplete({
                source: response,
                minLength: 3
            });
            console.log(((d.getTime() - start_time)/1000.0) + ' seconds');
        },

        complete: function (data) {
            console.log("Done!");
        },
    })
};

function ClearAll() {
    var objSel = document.forms['f1'].mySelect;

    if (document.forms["f1"].name1.length>="") {
        document.forms["f1"].name1.value = "";
    }

}


