window.CSPOR = window.CSPOR || {};

CSPOR.projectActions = {
	
	objId: null,
	
	editableLayersCount: null,
	
	initPopup: function(){

		objId = (parseInt((Math.random()) * (100000000 - 1) + 1)).toString();  // Creating a random number from 1 to 100000000 for popup'ID
		var self = this;
		
		$('body').append('<div id="pa_popup" style="display: none;">' +      //Method "append" adds all html structure
			'<p>' +
			'<select id = "pa_combbox" ></select>' +
			'</p>'+

			'<table id = "pa_tab1">' +
			'<tr>' +
			'<td><input type = "button" id = "pa_bplus" value = "+" /></td>' +
			'<td><input type = "button" id = "pa_bminus" value = "-" /></td>' +
			'</tr>' +
			'</table>' +

			'<div id = "pa_div4xy"><table id = "pa_tabxy"></table></div>' +

			'<p><input type = "button" id="pa_bdraw" value = "Draw" /></p>' +


			'<div id="pa_lastdiv">' +
			'<table id="pa_tab2">' +
			'<tr>' +
			'<td><input type = "button" id = "pa_bclear" value = "Clear"/></td>' +
			'<td><input type = "button" id = "pa_bclose" value = "Close"/></td>' +
			'</tr>' +
			'</table>' +
			'</div>'+
			'</div>');
		
		this.doStyle();             // Purpose of this procedure is to start a creation style properties with css
		this.popCenter();              // This procedure makes popup in the middle of screen
		this.fillingComboout()			// And this one filling values of combobox out)
		
		$('#pa_bplus').bind('click',function addstr() {        // Handler for button "+"
			if ($("#pa_combbox").val() != 'POINT') {
				$("#pa_tabxy").append('<tr class="stringss">' +
					'<td>X:</td><td><input type="number" class="wrap CellforX" /></td>' +
					'<td>Y:</td><td><input type="number" class="wrap CellforY" /></td>' +
					'</tr>');
				
				$('.wrap').css({"width": "80px", "border-radius": "4px", "border": "3px solid white", "-webkit-box-shadow": "inset 0 0 2px  rgba(0,0,0,0.1),0 0 2px rgba(0,0,0,0.1)", "-moz-box-shadow": "inset 0 0 2px  rgba(0,0,0,0.1), 0 0 2px rgba(0,0,0,0.1)", "box-shadow": "inset 0 0 2px  rgba(0,0,0,0.1), 0 0 2px rgba(0,0,0,0.1)", "background": "rgba(255,255,255,0.4)"});
			}
		});

		$('#pa_bminus').bind('click',function deletestr() {       // Handler for button "-"       (($("#pa_combbox").val() != 'POINT') &&
			if ($('.stringss').length != 1){
				$('.stringss:last').remove()
			}
		});

		$('#pa_bdraw').bind('click',function drawing() {          // Handler for button "Draw"
			self.validation();
		});

		$('#pa_combbox').bind('change',function comboChange(){     // Handler for Combobox(<select>) which starts to work when the user changes values in this element
			$(".stringss").detach();
			self.initCells();
		});

		$('#pa_bclear').bind('click',function clear(){             //Clear all entry fields
			$(".stringss").detach();
			self.initCells();
		});

		$('#pa_bclose').bind('click',function close(){       //This procedure willclose the popup
			
			PortalApp.api.deleteMapObject(objId);
			
			$('#pa_bdraw').off();
			$('#pa_combbox').off();
			$('#pa_bclear').off();
			$('#pa_bminus').off();
			$('#pa_bplus').off();
			$('#pa_bclose').off();
			
			$('#pa_popup').detach();
		});
		
		//open the popup when everything is prepared		
		_wait = function() {	
			if (self.editableLayersCount == null) {
				setTimeout(function() {
					_wait.call(self)
				}, 50);
			} 
			else if (this.editableLayersCount > 0) {
				$('#pa_popup').draggable();
				$('#pa_popup').show();
			}
			else {
				alert('There are no editable layers to work with');
			} 
			return;
		};
		
		_wait.call(this);

	},
	
	openEditorPopup: function(){
		
		this.initPopup();
	},

	fillingComboout: function(){          // Here is a creating a get-query
		
		var self = this,
			projectId = 40;
			url = "http://cstrade.koenig.ru:7001/UrbanViewAPI/rest/metadata/project/editables?project_id=" + projectId;
			
		$.getJSON(url,function() {
			})
			.done(function(data) {

				var i = 0,
					o = data["metadataProjectList"][0]["metadataLayerList"];
				
				self.editableLayersCount = o.length;

				while (i < o.length){
					document.getElementById("pa_combbox").options[i] = new Option(o[i]["themeLabel"],o[i]["geometryType"]);
					i++;
				};
				self.initCells();
			})
			.fail(function() {
				console.log( "error" );
			});
		
	},

	validation: function(){             //There are procedures of validation data in this part of code


		var e = true;
		var t = 0;
		var z = $(".stringss").length;
		var a,b;

			while ( e && t<=z) {

				var varx = $(".CellforX");
				var vary = $(".CellforY");

			if (varx.eq(t).val()=="" || vary.eq(t).val()=="") {
				e = false;
				alert("There are empty values");
				return;
			}
			a = parseInt(varx.eq(t).val(),10);
			b = parseInt(vary.eq(t).val(),10);

			if (b < -90 || b > 90){
				e = false;
				alert("There is an invalid value for coordinate Y");
				return;
			}
			if (a < -180 || a > 180){
				e = false;
				alert("There is an invalid value for coordinate X");
				return;
			}
			t++;
		}
			if (e == true){
				this.arrayCreation();           // If validation was successfull - next step - call a procedure of creating an array of coordiantes
			}

	},

	arrayCreation: function(){                      // The main result - an array of number with coordiantes. For example [12.24.0.33.45.0....n.n.0] z coordinate is null
		
		var coords = [];

		for (i=0;i<$(".stringss").length;i++) {
			coords.push(parseFloat($(".CellforX").eq(i).val()));
			coords.push(parseFloat($(".CellforY").eq(i).val()));
			coords.push(0);

		}
		if ($("#pa_combbox").val()=="POLYGON"){

			coords.push(coords[0]);
			coords.push(coords[1]);
			coords.push(coords[2]);
		}
		PortalApp.api.updateMapObject(objId, coords);
	},

	initCells: function(){
		var layerName = $('#pa_combbox option:selected').text();
		PortalApp.api.startMapEditor(layerName);
		$("#pa_tabxy").append('<tr class="stringss">' +
			'<td>X:</td><td><input type="number" class="wrap CellforX" /></td>' +
			'<td>Y:</td><td><input type="number" class="wrap CellforY" /></td>' +
			'</tr>');
		
		$('.wrap').css({"width": "80px", "border-radius": "4px", "border": "3px solid white", "-webkit-box-shadow": "inset 0 0 2px  rgba(0,0,0,0.1),0 0 2px rgba(0,0,0,0.1)", "-moz-box-shadow": "inset 0 0 2px  rgba(0,0,0,0.1), 0 0 2px rgba(0,0,0,0.1)", "box-shadow": "inset 0 0 2px  rgba(0,0,0,0.1), 0 0 2px rgba(0,0,0,0.1)", "background": "rgba(255,255,255,0.4)"});
	},

	doStyle: function() {

		$('#pa_popup').css({"font-family": "Tahoma","border":"2px solid cornflowerblue","background-color":"#e4edf8","border-radius":"15px", "height": "500px", "width":"400px","position": "absolute"});
		$('#pa_combbox').css({"width":"190px","margin-left":"98px","margin-top":"10px","border-radius":"5px","height":"30px", "border":"1px solid azure"});
		$('#pa_tab1').css({"margin-left":"20px","margin-top":"20px","border-spacing":"20px 0","border-collapse":"separate"});
		$('#pa_div4xy').css({"margin-top":"10px","margin-left":"20px","margin-right":"20px","background-color": "rgb(178, 203, 234)","border":"1px solid azure","height": "250px","border-radius":"10px","overflow":"auto"});
		$('#pa_tab2').css({"margin-left":"160px","margin-top":"8px","border-spacing":"10px 0","border-collapse":"separate"});
		$('#pa_tabxy').css({"margin-left":"40px","cellspacing":"50px","border-spacing":"10px 5px","border-collapse":"separate"});
		$('#pa_lastdiv').css({"padding":"14px"});


		var allbuttons=$('#pa_bplus,#pa_bminus,#pa_bclear,#pa_bclose,#pa_bdraw');
		allbuttons.css({"line-height": "5px","border-radius":"6px","background-color":"#4D9AEF","color": "white", "border": "1px", "cursor": "pointer"});


		$('#pa_bclear,#pa_bclose').css({"width":"80px","height":"25px"});
		$('#pa_bplus,#pa_bminus').css({"width":"60px","height":"20px"});
		$('#pa_bdraw').css({"width":"100px","height":"25px","margin-left":"40px","margin-top":"20px"});

		allbuttons.mouseenter(function( event ) {
			$(this).css({"background-color":"#778899"});
		});

		allbuttons.mouseleave(function( event ) {
			$(this).css({"background-color":"#1E90FF"});
		});

	},

	popCenter: function(){
		var winWidth = $(window).width();
		var winHeight = $(document).height();
		var scrollPos = $(window).scrollTop();

		var disWidth = winWidth - 450;
		var disHeight = scrollPos + 50;

		$('#pa_popup').css({'left' : disWidth+'px', 'top' : disHeight+'px'});
	}

};
